#!/usr/bin/env ruby

require_relative 'controllers/arduino_controller.rb'
require_relative 'lib/arduino_serial.rb'
require 'serialport'

info_methods = [:sys_info,
                :cpu_usage,
                :ram_info,
                :net_info,
                :media_metadata]

loop do
  p info_methods.map{|m| ArduinoController.instance.send m}
  sleep 1
end

# port_str = '/dev/ttyACM0'
# baud_rate = 9600
# data_bits = 8
# stop_bits = 1

# parity = SerialPort::NONE

# sp = SerialPort.new(port_str, baud_rate, data_bits, stop_bits)
# debugger

# sp.putc('h')
# sp.putc('e')
# sp.putc('l')
# sp.putc('o')
 
