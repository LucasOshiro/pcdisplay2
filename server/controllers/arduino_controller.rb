require 'singleton'
require 'byebug'
require 'json'
require 'elparser'
require_relative '../lib/arduino_serial.rb'
require_relative '../models/pc_info.rb'

class ArduinoController
  include Singleton

  def initialize
    @port_str = '/dev/ttyACM0'
    @baud_rate = 9600

    @arduino = Arduino.new nil, nil
    @pc_info = PCInfo.instance

    @current_state = :cpu_usage

    @display_data = [{type: '', val: ''},
                     {type: '', val: ''}]
  end

  def debug
    debugger
  end

  def sys_info
    system_name = @pc_info.sys_info[:NAME]
    pc_name = @pc_info.pc_name
    ["SYSINFO", system_name, pc_name]
  end

  def cpu_usage
    usage = @pc_info.cpu_usage.*(100).truncate
    ["CPUUSAGE", usage.to_s]
  end

  def ram_info
    ram_info = @pc_info.ram_info
    ["RAMINFO",
     ram_info[:totalMem].to_s,
     ram_info[:usedMem].to_s,
     ram_info[:totalSwp].to_s,
     ram_info[:usedSwp].to_s]
  end
  
  def net_info
    net_info = @pc_info.net_info
    ["NETINFO",
     net_info[:upSpeed].to_s,
     net_info[:downSpeed].to_s]
  end

  def media_metadata
    mdata = @pc_info.media_metadata
    ["METADATA",
     mdata[:title].to_s,
     mdata[:artist].to_s,
     mdata[:album].to_s,
     mdata[:track].to_s]
  end
end
