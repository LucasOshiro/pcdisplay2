require 'singleton'
require 'dbus'
require 'byebug'

class PCInfo
  include Singleton
  attr_reader :sys_info,
              :pc_name,
              :cpu_usage,
              :ram_info,
              :net_info,
              :player,
              :media_metadata,
              :cpu_temperature

  attr_writer :interval

  def initialize
    @sys_info = `cat /etc/os-release`.lines.map do |line|
      x = line.gsub("\n", '').split('=')
      {x[0].to_sym => x[1].gsub('"', '')}
    end.reduce(:merge)

    @pc_name = `hostname`.gsub("\n", '')
    @interval = 1

    auto_refresh
  end

  def auto_refresh
    [:refresh_CPU,
     :refresh_RAM,
     :refresh_net,
     :refresh_media,
     :refresh_CPU_temperature
    ].each do |refreshMethod|
      self.send refreshMethod
      Thread.new do
        loop do
          self.send refreshMethod
          sleep @interval
        end
      end
    end
  end

  def refresh_CPU
    cpu_stat = `cat /proc/stat`.lines[0].split[1..-1].map {|v| v.to_f}
    old_total_cpu_time = cpu_stat.reduce(:+)
    old_total_cpu_usage = old_total_cpu_time - cpu_stat[3]

    sleep(1)

    cpu_stat = `cat /proc/stat`.lines[0].split[1..-1].map {|v| v.to_f}
    new_total_cpu_time = cpu_stat.reduce(:+)
    new_total_cpu_usage = new_total_cpu_time - cpu_stat[3]
    
    @cpu_usage =  (new_total_cpu_usage - old_total_cpu_usage) / (new_total_cpu_time - old_total_cpu_time)
  end

  def refresh_RAM
    free = `free`.lines.map{|line| line.split}
    @ram_info = {
      totalMem: free[1][1],
      usedMem:  free[1][2],
      totalSwp: free[2][1],
      usedSwp:  free[2][2]
    }
  end

  def refresh_net
    dt = 1
    table = nil
    
    file = File.new('/proc/net/dev', 'r')
    table = file.readlines[2..-1].map{|line| line.split[1..-1].map{|val| val.to_i}}
    file.close

    old_sum_down = table.inject(0){|sum, line| sum + line[0]}
    old_sum_up   = table.inject(0){|sum, line| sum + line[8]}

    sleep dt

    file = File.new('/proc/net/dev', 'r')
    table = file.readlines[2..-1].map{|line| line.split[1..-1].map{|val| val.to_i}}
    file.close

    new_sum_down = table.inject(0){|sum, line| sum + line[0]}
    new_sum_up   = table.inject(0){|sum, line| sum + line[8]}

    @net_info = {
      upSpeed: (new_sum_up - old_sum_up) / dt,
      downSpeed: (new_sum_down - old_sum_down) / dt
    }
  end

  def refresh_media
    @media_metadata = {}
    bus = DBus::SessionBus.instance
    player_names = bus.proxy.ListNames[0].select{|name| name.match(/^org\.mpris\.MediaPlayer2/)}
    
    if player_names.empty?
      @players = nil
      return
    end

    @players = player_names.map{|name| bus.service(name).object('/org/mpris/MediaPlayer2')}

    @interfaces = @players.map{|player| player['org.mpris.MediaPlayer2.Player']}

    @player = @interfaces[0]

    @media_metadata = @players.map do |player|
      m = player['org.freedesktop.DBus.Properties']
      if m.nil? || !m.respond_to?(:Get) then
        nil
      else
        meta = m.Get 'org.mpris.MediaPlayer2.Player', 'Metadata'
        meta && meta[0] && {title:  meta[0]['xesam:title'],
                            artist: meta[0]['xesam:artist'],
                            album:  meta[0]['xesam:album'],
                            track:  meta[0]['xesam:trackNumber']
        }
      end
    end
  end

  def refresh_CPU_temperature
    sensors_output = `sensors -u`
    temps = sensors_output.lines
              .select{|line| line.match?(/^  temp[0-9]+_input/)}
              .map{|line| line.split[1].to_i}

    @cpu_temperature = temps[0]
  end
  
  def time
    Time.now
  end

end
