require 'serialport'

class Arduino
  def initialize(port_str, baud_rate)
    @port_str = port_str || '/dev/ttyACM0'
    @baud_rate = baud_rate || 9600
    @data_bits = 8
    @stop_bits = 1

    @arduino_serialport = SerialPort.new(@port_str,
                                         @baud_rate,
                                         @data_bits,
                                         @stop_bits)

  end

  def send_string s
    @arduino_serialport.print s
  end

  def << s
    send_string s
  end

  def read_string
    @arduino_serialport.gets 
  end

  def >> s
    s << read_string
  end
  
end
