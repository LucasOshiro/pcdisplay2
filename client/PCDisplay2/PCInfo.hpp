#ifndef _PCINFO
#define _PCINFO
#include "LispListParser.hpp"

struct PCInfo {
    char systemName[32];
    char pcName[32];

    int cpuUsage;
    
    int totalRAM;
    int usedRAM;
    int totalSwap;
    int usedSwap;

    long upSpeed;
    long downSpeed;
};

extern PCInfo info;

typedef void (*rule)(LispList *);

const char contentTypes[][30] = {
    "SYSINFO",
    "CPUUSAGE",
    "RAMINFO",
    "NETINFO",
    "METADATA"
};

void getInfoFromLispList (LispList *l);

void sysInfoRule  (LispList *l);
void CPUUsageRule (LispList *l);
void RAMInfoRule  (LispList *l);
void netInfoRule  (LispList *l);
void metadataRule (LispList *l);

const rule rules[] = {
    sysInfoRule,  
    CPUUsageRule, 
    RAMInfoRule,  
    netInfoRule,  
    metadataRule 
};

#endif
