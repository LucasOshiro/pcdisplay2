#include "LispListParser.hpp"
#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include "Arduino.h"

static LispListData *getDataAt (InnerList lst, int index);
static void InnerListDestroy (InnerList list);
static void InnerListPrint (InnerList list);
static LispListData *parseElement (char *raw_str);
static void fillElementsArray (LispListData **&arr, InnerList list, int index);

LispListData::~LispListData () {}

void LispListData::print () {}

LispListAtom::LispListAtom (char *str) {
    this->str = new char[strlen (str) + 1];
    strcpy (this->str, str);
}

LispListAtom::~LispListAtom () {
    delete[] this->str;
}

void LispListAtom::print () {
    Serial.write('"');
    Serial.write(this->str);
    Serial.write('"');
}

LispListElement::LispListElement (LispListData *data) {
    this->data = data;
    this->next = NULL;
}

LispListElement::LispListElement (char *data) {
    this->data = new LispListAtom (data);
    this->next = NULL;
}

LispList::LispList () {
    this->list = NULL;
    this->last = NULL;
    this->len = 0;
}

LispList::LispList (char *raw_str) {
    int p = 0;
    bool q = false;

    char *buffer = new char[strlen (raw_str) + 1];
    char *w = buffer;

    this->list = NULL;
    this->last = NULL;
    this->len = 0;

    for (char *c = raw_str; *c != '\0'; c++) {
        if (q) {

            *(w++) = *c;
            if (*c == '"') q = !q;
        }
        else {
            switch (*c) {
            case '"':
                q = !q;
                *(w++) = *c;
                break;

            case '(':
                if (p > 0) *(w++) = *c;
                p++;
                break;

            case ')':
                *(w++) = *c;
                if (p == 1) {
                    *(w - 1) = '\0';
                    w = buffer;
                    this->append (parseElement (buffer));
                }
                p--;
                break;

            case ' ':
                if (p == 1) {
                    *(w++) = '\0';
                    w = buffer;
                    this->append (parseElement (buffer));
                }
                else *(w++) = *c;
                break;

            default:
                *(w++) = *c;
            }

        }
    }

    delete[] buffer;
}

LispList::~LispList () {
    InnerListDestroy (this->list);
}

void LispList::append (LispListData *data) {
    if (data == NULL) return;
    LispListElement *el = new LispListElement (data);
    if (this->last) this->last->next = el;
    this->last = el;
    if (list == NULL) list = last;
    this->len++;
}

void LispList::append (char *data) {
    LispListElement *el = new LispListElement (data);
    if (this->last) this->last->next = el;
    this->last = el;
    if (list == NULL) list = last;
    this->len++;
}

void LispList::print () {
    InnerListPrint (this->list);
}

LispListData *LispList::operator[] (int index) {
    return getDataAt (this->list, index);
}

int LispList::length () {
    return this->len;
}

LispListData **LispList::getElementsArray () {
    LispListData **arr = new LispListData *[this->len];
    fillElementsArray (arr, this->list, 0);
    return arr;
}

static void fillElementsArray (LispListData **&arr, InnerList list, int index) {
    if (list == NULL) return;
    arr[index] = list->data;
    fillElementsArray (arr, list->next, index + 1);
}

static LispListData *getDataAt (InnerList list, int index) {
    if (list == NULL) return NULL;
    if (index == 0) return list->data;
    return getDataAt (list->next, index - 1);
}

static void InnerListDestroy (InnerList list) {
    if (list == NULL) return;

    InnerList next = list->next;
    delete list->data;
    delete list;
    InnerListDestroy (next);
}

static void InnerListPrint (InnerList list) {
    Serial.write ('(');
    for (LispListElement *el = list; el != NULL; el = el->next) {
        el->data->print();
        Serial.write (' ');
    }
    Serial.write (')');
}


static LispListData *parseElement (char *raw_str) {
    LispListData *el = NULL;

    int raw_str_length = strlen(raw_str);
    int buffer_length = raw_str_length - 2;

    if (raw_str_length == 0) return NULL;

    char *buffer = new char[buffer_length + 1];
    strncpy (buffer, raw_str + 1, buffer_length);
    buffer[buffer_length] = '\0';

    switch (raw_str[0]) {
    case '"':
        el = new LispListAtom (buffer);
        break;

    case '(':
        el = new LispList (raw_str);
        break;
    }

    delete[] buffer;
    return el;
}

