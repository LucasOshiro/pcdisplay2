#ifndef _LISPLISTPARSER
#define _LISPLISTPARSER

class LispListData {
public:
    virtual ~LispListData ();
    virtual void print ();
};

class LispListAtom : public LispListData {
public:
    char *str;
    LispListAtom (char *str);
    ~LispListAtom ();

    void print ();
};

class LispListElement {
public:
    LispListData *data;
    LispListElement *next;

    LispListElement (LispListData *data);
    LispListElement (char *data);
};

typedef LispListElement *InnerList;

class LispList : public LispListData {
private:
    InnerList list;
    LispListElement *last;
    int len;

public:
    LispList ();
    LispList (char *raw_str);

    ~LispList ();
    
    void append (LispListData *data);
    void append (char *element);
    void print ();

    void operator<< (LispListData *data) {this->append (data);}
    void operator<< (char *element) {this->append(element);}

    LispListData *operator[] (int index);

    int length();
    LispListData **getElementsArray ();
};



#endif
