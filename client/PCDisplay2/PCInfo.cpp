#include "PCInfo.hpp"
#include <string.h>
#include <stdio.h>
#include "Arduino.h"

PCInfo info;

void getInfoFromLispList (LispList *l) {
    LispListData **elements = l->getElementsArray ();
    LispListData **end = elements + l->length ();

    for (LispListData **element = elements; element < end; element++) {
        LispList *infoLispList = (LispList *) *element;
        LispListData **infoLispListElements = infoLispList->getElementsArray ();
        LispListData *first = infoLispListElements[0];

        char *contentString = ((LispListAtom *) first)->str;
        
        for (int i = 0; i < 5; i++) {
            if (strcmp (contentString, contentTypes[i]) == 0) {
                rule r = rules[i];
                r(infoLispList);
                break;
            }
        }
    }

    delete[] elements;
}

void sysInfoRule (LispList *l) {
    LispListAtom *sysNameElement = (LispListAtom *) (*l)[1];
    LispListAtom *pcNameElement  = (LispListAtom *) (*l)[2];

    strcpy (info.systemName, sysNameElement->str);
    strcpy (info.pcName, pcNameElement->str);
}

void CPUUsageRule (LispList *l) {
    LispListAtom *usageElement = (LispListAtom *) (*l)[1];
    sscanf (usageElement->str, "%d", &(info.cpuUsage));
}

void RAMInfoRule (LispList *l) {
    LispListAtom *trElement = (LispListAtom *) (*l)[1];
    LispListAtom *urElement = (LispListAtom *) (*l)[2];
    LispListAtom *tsElement = (LispListAtom *) (*l)[3];
    LispListAtom *usElement = (LispListAtom *) (*l)[4];

    sscanf (trElement->str, "%d", &(info.totalRAM));
    sscanf (urElement->str, "%d", &(info.usedRAM));
    sscanf (tsElement->str, "%d", &(info.totalSwap));
    sscanf (usElement->str, "%d", &(info.usedSwap));
}

void netInfoRule (LispList *l) {
    LispListAtom *upElement   = (LispListAtom *) (*l)[1];
    LispListAtom *downElement = (LispListAtom *) (*l)[2];

    sscanf (upElement->str,   "%ld", &(info.upSpeed));
    sscanf (downElement->str, "%ld", &(info.downSpeed));
}

void metadataRule (LispList *l) {
    
}
