#include "specialCharacters.h"
#include <LiquidCrystal.h>
#include "LispListParser.hpp"
#include <string.h>
#include "PCInfo.hpp"

LiquidCrystal lcd (12, 11, 5, 4, 3, 2);

void printCenter (int l, char *s) {
    int spc = (16 - strlen (s)) / 2;
    lcd.clear ();
    lcd.setCursor (spc, l);
    lcd.write (s);
}

char *getStringFromSerial () {
    char buffer[256];
    char *c;
    for (c = buffer;; c++) {
        while (!Serial.available ());
        *c = Serial.read();
        if (*c == '\n') break;
    }
    *c = '\0';

    char *s = new char[strlen(buffer) + 1];
    strcpy (s, buffer);
    return s;
}

void setup () {
    Serial.begin (9600);
    
    lcd.createChar (0, charFilledBlock);
    lcd.createChar (1, charBeginEmptyBlock);
    lcd.createChar (2, charMidEmptyBlock);
    lcd.createChar (3, charEndEmptyBlock);
    lcd.createChar (4, charDegrees);
    
    
    pinMode (13, OUTPUT);
    digitalWrite (13, LOW);
    lcd.begin (16, 2);
    delay (500);
    
    lcd.setCursor (0, 0);
    lcd.print ("Cebola");
    lcd.setCursor (0, 0);
    delay (500);
}

void loop() {
    while (!Serial.available ());
    char *s = getStringFromSerial ();
    
    LispList *l = new LispList (s);

    l->print ();
    Serial.write ('\n');
    delay (1000);

    getInfoFromLispList (l);

    Serial.println(info.upSpeed);
    Serial.println(info.downSpeed);
    Serial.println(info.totalRAM);
    Serial.println(info.usedRAM);
    Serial.println(info.totalSwap);
    Serial.println(info.usedSwap);
    Serial.println(info.cpuUsage);

    delete[] s;
    delete l;
}
